<?php
require_once '../../../wp-blog-header.php';
require_once dirname(__FILE__) . "/vendor/autoload.php";

$ecommerce_api = new Sistecredito_Adapter_WoocommercePlatformApi();
$confirmation_controller = new Sistecredito_Controller_Confirmation($ecommerce_api);

$confirmation_response = $confirmation_controller->process();

if ($confirmation_response->is_gateway_confirmation_request) {
	header("Content-Type: application/json");
    echo json_encode([
        "status" => $confirmation_response->is_valid ? "VALID" : "ERROR",
        "errors" => $confirmation_response->errors,
    ]);
    exit;
}

$message = "";
$payment_details = [];
$notice_type = "info";

if (!$confirmation_response->is_valid) {
	$message = implode(", ", $confirmation_response->errors);
	$notice_type = "error";
} else {
	$message = "<p><strong>Order confirmed!</strong></p><br/>";
	$payment_details = [
        "Credit number" => $confirmation_response->order->get_credit_number(),
        "Value" => $confirmation_response->order->get_total(),
    ];
}

get_header('shop');

?>

<main id="site-content" role="main">
	<article class="page type-page status-publish hentry">
		<header class="entry-header has-text-align-center header-footer-group">
			<div class="entry-header-inner section-inner medium">
				<h1 class="entry-title">Checkout</h1>
			</div><!-- .entry-header-inner -->
		</header>

		<div class="post-inner thin ">
			<div class="entry-content">
				<div class="woocommerce-notices-wrapper">
					<div class="woocommerce-message woocommerce-<?php echo $notice_type ?>" role="alert">
						<?php echo $message ?>
						<?php foreach ($payment_details as $field => $value): ?>
							<p>
								<strong><?=__($field, 'woocommerce')?>: </strong><?=$value?>
							</p>
						<?php endforeach;?>
					</div>
				</div>
			</div>
		</div>
	</article>
</main>

<?php

get_footer('shop');
