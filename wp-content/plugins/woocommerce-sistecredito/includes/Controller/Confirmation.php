<?php

class Sistecredito_Controller_Confirmation
{

    private $_form_validations = [
        "TypeDocument" => [
            "filter" => FILTER_VALIDATE_REGEXP,
            "options" => ["regexp" => "/^(CC|CE)$/"],
        ],
        "IdDocument" => [
            "filter" => FILTER_VALIDATE_REGEXP,
            "options" => ["regexp" => "/^[0-9]+?$/"],
        ],
        "ValueToPay" => FILTER_VALIDATE_FLOAT,
        "OrderId" => FILTER_VALIDATE_INT,
        "CreditNumber" => FILTER_VALIDATE_INT,
        "Authentication" => [
            "filter" => FILTER_VALIDATE_REGEXP,
            "options" => ["regexp" => "/^.+?$/"],
        ],
        "TransactionStatus" => [
            "filter" => FILTER_VALIDATE_REGEXP,
            "options" => ["regexp" => "/^(Terminado|Error)$/"],
        ],
    ];

    private $_query_params_validation = [
        "transactionId" => [
            "filter" => FILTER_VALIDATE_REGEXP,
            "options" => ["regexp" => "/^.+?$/"],
        ],
        "orderId" => FILTER_VALIDATE_INT,
    ];

    /**
     * @var Sistecredito_Interface_EcommercePlatformApi
     */
    private $_ecommerce_api;

    public function __construct(Sistecredito_Interface_EcommercePlatformApi $ecommerce_api)
    {
        $this->_ecommerce_api = $ecommerce_api;
    }

    /**
     * @return Sistecredito_Model_Response
     */
    public function process()
    {
        $querystring_params = filter_input_array(INPUT_GET, $this->_query_params_validation);
        $request_body = file_get_contents("php://input");

        if (strtoupper($_SERVER['REQUEST_METHOD']) == 'POST' && !empty($request_body)) {
            $confirmation_request = json_decode($request_body, true);
            $confirmation_request = filter_var_array($confirmation_request, $this->_form_validations);
            return $this->_confirm_cart($confirmation_request);
        }

        return $this->_show_confirmation_msg_buyer($querystring_params);
    }

    /**
     * @return Sistecredito_Model_Response
     */
    private function _show_confirmation_msg_buyer($querystring_params)
    {
        $response = new Sistecredito_Model_Response();

        if(empty($querystring_params)) {
            $response->is_valid = false;
            $response->errors[] = 'TransactionID/OrderID is empty';

            return $response;
        }

        $querystring_has_errors = array_reduce($querystring_params, function ($is_there_errors, $next) {
            if (empty($next)) {
                $is_there_errors = true;
            }

            return $is_there_errors;
        }, false);

        if ($querystring_has_errors) {
            $response->is_valid = false;
            $response->errors[] = 'TransactionID/OrderID is empty';

            return $response;
        }

        $order = $this->_ecommerce_api->get_order_object($querystring_params["orderId"]);

        if (!$order->is_order_confirmed()) {
            $response->is_valid = false;
            $response->errors[] = 'The order wasn\'t confirmed';

            return $response;
        }

        $response->is_valid = true;
        $response->order = $order;

        return $response;
    }

    /**
     * @return Sistecredito_Model_Response
     */
    private function _confirm_cart($confirmation_request)
    {
        $response = new Sistecredito_Model_Response();
        $response->is_gateway_confirmation_request = true;
        $is_there_errors = array_reduce($confirmation_request, function ($is_there_errors, $next) {
            if (empty($next)) {
                $is_there_errors = true;
            }

            return $is_there_errors;
        }, false);

        if ($is_there_errors || $confirmation_request == null) {
            $response->is_valid = false;
            $response->errors[] = 'The gateway rejected the payment request. Please try with another payment method';

            return $response;
        }

        try {
            $jwt_key = Sistecredito_Helper::get_jwt_key((int) $confirmation_request["OrderId"]);
            $decoded_jwt_token = \Firebase\JWT\JWT::decode($confirmation_request["Authentication"], $jwt_key, array('HS256'));

            if ($decoded_jwt_token->aud !== $this->_ecommerce_api->get_site_url()) {
                $response->is_valid = false;
                $response->errors[] = sprintf('The JWT token has a different audience (%s) than expected (%s)', $decoded_jwt_token->aud, $this->_ecommerce_api->get_site_url());

                return $response;
            }

            $this->_ecommerce_api->add_log_entry($confirmation_request["OrderId"], sprintf('Received JWT Token: %s', json_encode($decoded_jwt_token)));

        } catch (Exception $e) {
            $this->_ecommerce_api->add_log_entry($confirmation_request["OrderId"], sprintf('Error validating JWT Token: %s', $e->getMessage()));
            $response->is_valid = false;
            $response->errors[] = "The token is invalid";

            return $response;
        }

        $order = $this->_ecommerce_api->get_order_object($confirmation_request["OrderId"]);

        if ((float) $order->get_total() !== (float) $confirmation_request["ValueToPay"]) {
            $error_msg = sprintf('Value received from gateway (%f) does not match with the cart total (%f)', (float) $confirmation_request["ValueToPay"], $order->get_total());

            $this->_ecommerce_api->add_log_entry($confirmation_request["OrderId"], $error_msg);

            $response->is_valid = false;
            $response->errors[] = $error_msg;

            return $response;
        }

        $order->confirm_order($confirmation_request["CreditNumber"]);

        $this->_ecommerce_api->add_log_entry($confirmation_request["OrderId"], sprintf('Order payment confirmation received from Sistecredito API. Generated Order ID: %s', $this->module->currentOrder));

        $response->is_valid = true;

        return $response;
    }

}
