<?php

class Sistecredito_Service_WoocommercePlatformApi implements Sistecredito_Interface_EcommercePlatformApi
{
    public function get_site_url()
    {
        return get_site_url();
    }

    public function add_log_entry($order_id, $message)
    {
        $order = wc_get_order($order_id);
        $order->add_order_note($message);
    }

    public function get_order_object($order_id)
    {
        $order = wc_get_order($order_id);
        return new Sistecredito_Adapter_WoocommerceOrder($order);
    }
}
