<?php
class Sistecredito_Service_APIHandler
{

    /**
     * Endpoint for requests to Sistecredito.
     *
     * @var string
     */
    private $endpoint = "";

    /**
     * Constructor.
     *
     * @param string $endpoint Sistecredito endpoint.
     */
    public function __construct($endpoint)
    {
        $this->endpoint = $endpoint;
    }

    /**
     * Return the URL where the user is redirected once WC confirms the checkout.
     *
     * @param array $credit_request_params Information about the credit to be requested to Sistecredito
     * @param array $rest_api_settings Information about the credit to be requested to Sistecredito
     */
    public function get_request_url($credit_request_params, $rest_api_settings)
    {
        $body = json_encode($credit_request_params);
        $jwt_token = Sistecredito_Helper::generate_jwt_token($credit_request_params["orderId"]);
        $post_request_params = [
            'body' => $body,
            'timeout' => '5',
            'redirection' => '5',
            'httpversion' => '1.0',
            'blocking' => true,
            'headers' => [
                "Content-Type" => "application/json",
                "Ocp-Apim-Subscription-Key" => $rest_api_settings["subscription_key"],
                "SCLocation" => "0,0",
                "country" => "co",
                "SCOrigen" =>  "Production",
                "Authentication" => $jwt_token
            ],
            'cookies' => array(),
        ];

        $http_response = wp_remote_post($this->endpoint, $post_request_params);

        if (isset($http_response["response"]) && isset($http_response["response"]["code"])) {
            $http_code = $http_response["response"]["code"];

            switch ($http_code) {
                case 200:{
                        $rest_api_response = $http_response["body"];
                        $response_object = json_decode($rest_api_response);
                        $final_url = $response_object->data->urlToRedirect . "?transactionId=" . $response_object->data->transactionId;
                        return $final_url;
                    }
                case 400:{
                        $rest_api_response = $http_response["body"];
                        $response_object = json_decode($rest_api_response);
                        $reason_rejection = $response_object->message;
                        wc_add_notice('Hubo un error al procesar el pago: ' . $reason_rejection, 'error');
                        WC_Wapp_Sistecredito::log('Sistecredito rejection: ' . $reason_rejection);
                        break;
                    }
            }

        }

        $error_detail = wc_print_r(array_merge($credit_request_params, array_intersect_key($rest_api_settings, $credit_request_params)), true);
        WC_Wapp_Sistecredito::log('Sistecredito Request Args for order ' . $credit_request_params["orderId"] . ': ' . $error_detail, "error");

        return '';
    }
}
