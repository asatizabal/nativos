<?php

class Sistecredito_Adapter_WoocommerceOrder implements Sistecredito_Interface_Order
{

    private $_order;

    public function __construct($order)
    {
        $this->_order = $order;
    }

    public function get_id()
    {
        return $this->_order->get_id();
    }

    public function get_total()
    {
        return $this->_order->get_total();
    }

    public function confirm_order($credit_number)
    {
        return $this->_order->payment_complete($credit_number);
    }
    
    public function is_order_confirmed()
    {
        return $this->_order->is_paid();
    }

    public function get_credit_number()
    {
        return $this->_order->get_transaction_id();
    }

}
