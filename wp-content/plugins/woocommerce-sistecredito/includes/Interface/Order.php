<?php

interface Sistecredito_Interface_Order {
    public function get_id();
    public function get_total();
    public function confirm_order($credit_number);
    public function is_order_confirmed();
    public function get_credit_number();
}