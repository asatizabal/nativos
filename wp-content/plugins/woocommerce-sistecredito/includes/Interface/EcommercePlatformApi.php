<?php

interface Sistecredito_Interface_EcommercePlatformApi {
    
    public function get_site_url();

    public function add_log_entry($order_id, $message);

    /**
     * @return Sistecredito_Interface_Order
     */
    public function get_order_object($order_id);
}