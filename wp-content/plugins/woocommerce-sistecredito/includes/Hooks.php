<?php

class Sistecredito_Hooks {

    const SISTECREDITO_TABLE_NAME = 'sistecredito';

    public static function post_install()
    {
        global $wpdb;

        $table_name = $wpdb->prefix . self::SISTECREDITO_TABLE_NAME;
        
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name (
            `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
            `date` TIMESTAMP NOT NULL,
            `order_id` INT( 11 ) UNSIGNED NOT NULL,
            `request_url` varchar(255),
            `credit_number` int(11),
            `action` varchar(255) not null,
            `request_token` varchar(50),
            PRIMARY KEY (`id`)
        ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }

}