<?php

class Sistecredito_Model_Response {
    /**
     * @var Sistecredito_Interface_Order
     */
    public $order;

    /**
     * @var boolean
     */
    public $is_valid = false;

    /**
     * @var array
     */
    public $errors = [];

    /**
     * @var boolean
     */
    public $is_gateway_confirmation_request;
}