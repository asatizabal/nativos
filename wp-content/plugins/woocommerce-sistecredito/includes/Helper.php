<?php

class Sistecredito_Helper
{

    private static $_validations = [
        "typeDocument" => [
            "filter" => FILTER_VALIDATE_REGEXP,
            "options" => ["regexp" => "/^(CC|CE)$/"],
        ],
        "idDocument" => [
            "filter" => FILTER_VALIDATE_REGEXP,
            "options" => ["regexp" => "/^[0-9]{4,20}$/"],
        ],
        "valuetopay" => FILTER_VALIDATE_FLOAT,
        "orderid" => FILTER_VALIDATE_INT,
        "creditnumber" => FILTER_VALIDATE_INT,
    ];

    private static $_query_params_validation = [
        "transactionId" => [
            "filter" => FILTER_VALIDATE_REGEXP,
            "options" => ["regexp" => "/^.+?$/"],
        ],
        "orderId" => FILTER_VALIDATE_INT,
    ];

    private static $_validation_messages = [
        "typeDocument" => "The document type should be: CC, CE",
        "idDocument" => "The document number should be a number and shouldn\'t contain more than 20 digits",
        "valueToPaid" => "The value to paid should be a number",
    ];

    /**
     * Validate the request that comes from a POST request
     * @return Sistecredito_Model_Response Response received from Sistecredito Gateway
     */
    public static function validate_request()
    {
        $querystring_params = filter_input_array(INPUT_GET, self::$_query_params_validation);
        $request_body = file_get_contents("php://input");
        $response = new Sistecredito_Model_Response();
        $response->is_valid = false;

        if (strtoupper($_SERVER['REQUEST_METHOD']) == 'POST' && !empty($request_body)) {
            $confirmation_request = json_decode($request_body, true);
            $confirmation_request = filter_var_array($confirmation_request, self::$_validations);
        }

        if (empty($post_request_values["orderid"])) {
            $response->errors[] = __('The order id is empty', 'woocommerce');
            return $response;
        }

        $response->order = wc_get_order($post_request_values["orderid"]);
        if (empty($response->order)) {
            $response->errors[] = __('The order ID was not found', 'woocommerce');
            return $response;
        }

        if (empty($post_request_values["creditnumber"]) || $post_request_values["creditnumber"] == "0") {
            $response->errors[] = __('The credit wasn\'t approved.', 'woocommerce');
            return $response;
        }

        $fields_names = array_keys($post_request_values);
        foreach ($fields_names as $field_name) {
            if ($post_request_values[$field_name] === null) {
                $response->errors[] = sprintf(__("'%s' is a required field", 'woocommerce'), $field_name);
            } else if ($post_request_values[$field_name] === false) {
                $response->errors[] = __(self::$_validation_messages[$field_name], 'woocommerce');
            }
        }

        if (count($response->errors) === 0) {
            $response->is_valid = true;
            $response->credit_number = $post_request_values["creditnumber"];
            $response->credit_value = $post_request_values["valuetopay"];
        }

        return $response;
    }

    public static function generate_jwt_token($order_id)
    {
        $jwt_key = uniqid("sistecredito", true);

        self::save_jwt_key($order_id, $jwt_key);

        $jwt_payload = array(
            "iss" => get_site_url(),
            "aud" => get_site_url(),
            "iat" => time(),
            "nbf" => strtotime("+30 seconds"),
        );

        $jwt_token = \Firebase\JWT\JWT::encode($jwt_payload, $jwt_key);

        return $jwt_token;
    }

    public static function get_jwt_key(int $order_id)
    {
        global $wpdb;

        $table_name = $wpdb->prefix . Sistecredito_Hooks::SISTECREDITO_TABLE_NAME;

        $result = $wpdb->get_results("SELECT request_token FROM $table_name WHERE order_id = $order_id ORDER BY date DESC");

        return count($result) > 0 ? $result[0]->request_token : null;
    }
    
    public static function save_jwt_key($order_id, $request_token)
    {
        global $wpdb;

        $table_name = $wpdb->prefix . Sistecredito_Hooks::SISTECREDITO_TABLE_NAME;

        $wpdb->insert(
            $table_name,
            array(
                'order_id' => $order_id,
                'action' => Sistecredito_Constants_GatewayActions::TOKEN_GENERATED,
                'request_token' => $request_token,
            )
        );
    }
}
