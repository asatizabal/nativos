<?php
/*
Plugin Name: WooCommerce Sistecredito Gateway
Plugin URI: http://www.waplicaciones.co/
Description: Plugin de integracion entre Wordpress-Woocommerce con Sistecredito
Version: 1.0
Author: Waplicaciones SAS
Author URI: http://www.waplicaciones.co/
 */

require_once dirname(__FILE__) . "/vendor/autoload.php";

add_action('plugins_loaded', 'woocommerce_wa_sistecredito_gateway', 0);

register_activation_hook( __FILE__, 'Sistecredito_Hooks::post_install' );

function woocommerce_wa_sistecredito_gateway()
{
    if (!class_exists('WC_Payment_Gateway')) {
        return;
    }

    class WC_Wapp_Sistecredito extends WC_Payment_Gateway
    {

        /**
         * Logger instance
         *
         * @var WC_Logger
         */
        public static $log = false;

        /**
         * Whether or not logging is enabled
         *
         * @var bool
         */
        public static $log_enabled = true;

        /**
         * Constructor de la pasarela de pago
         *
         * @access public
         * @return void
         */
        public function __construct()
        {
            $this->id = 'wasistecredito';
            $this->icon = apply_filters('woocomerce_wasistecredito_icon', plugins_url('/img/logoSistecredito.png', __FILE__));
            $this->has_fields = true;
            $this->method_title = 'wasistecredito';
            $this->method_description = 'Integración de Woocommerce a la pasarela de pagos Sistecredito';

            $this->init_form_fields();
            $this->init_settings();

            $this->title = $this->settings['title'];
            $this->vendor_id = $this->settings['vendor_id'];
            $this->store_id = $this->settings['store_id'];
            $this->gateway_url = $this->settings['gateway_url'];
            $this->test = $this->settings['test'];
            $this->document_type_list = array(
                ["typeId" => "CC", "typeName" => "Cedula de Ciudadania"],
                ["typeId" => "CE", "typeName" => "Cedula de Extranjeria"],
            );
            self::$log_enabled = $this->test;

            if (version_compare(WOOCOMMERCE_VERSION, '2.0.0', '>=')) {
                add_action('woocommerce_update_options_payment_gateways_' . $this->id, array(&$this, 'process_admin_options'));
            } else {
                add_action('woocommerce_update_options_payment_gateways', array(&$this, 'process_admin_options'));
            }
            add_action('woocommerce_receipt_wasistecredito', array(&$this, 'receipt_page'));
            add_action('woocommerce_api_wasiscredito_confirmation', array(&$this, 'confirm_payment' ) );

        }

        public function payment_fields()
        {
            $fields = array();
            $documentTypeToStringFunction = function ($prev, $documentType) {
                $prev = $prev . "<option value=\"{$documentType['typeId']}\">{$documentType['typeName']}</option>";
                return $prev;
            };
            $fieldsAsStringFunction = function ($prev, $field) {
                $prev = $prev . $field;
                return $prev;
            };

            $document_type_dropdown_options = array_reduce($this->document_type_list, $documentTypeToStringFunction, '');

            $default_fields = array(
                'document-type' => '<p class="form-row form-row-first">
					<label for="' . esc_attr($this->id) . '-document-type">' . esc_html__('Tipo Documento', 'woocommerce') . '&nbsp;<span class="required">*</span></label>
					<select id="' . esc_attr($this->id) . '-document-type" class="document-type-list" maxlength="9" autocomplete="off" name="' . esc_attr($this->id) . '-document-type">' . $document_type_dropdown_options . '</select>
				</p>',
                'document-id' => '<p class="form-row form-row-last">
					<label for="' . esc_attr($this->id) . '-document-id">' . esc_html__('Numero Documento', 'woocommerce') . '&nbsp;<span class="required">*</span></label>
					<input id="' . esc_attr($this->id) . '-document-id" class="input-text" type="text" autocomplete="off" name="' . esc_attr($this->id) . '-document-id" maxlength="15" />
				</p>',
            );

            $fields = wp_parse_args($fields, apply_filters('woocommerce_wasistecredito_form_fields', $default_fields, $this->id));
            $fieldsAsString = array_reduce($fields, $fieldsAsStringFunction, '');
            ?>
			<fieldset id="<?php echo esc_attr($this->id) ?>-cc-form" class='wc-payment-form'>
				<?php echo do_action('woocommerce_wasistecredito_form_start', $this->id) ?>
				<?php echo $fieldsAsString ?>
				<?php echo do_action('woocommerce_wasistecredito_form_end', $this->id) ?>
				<div class="clear"></div>
			</fieldset>
			<script>jQuery('.document-type-list').selectWoo({ width: '100%' });</script>
			<?php
		}

        /**
         * Funcion que define los campos que iran en el formulario en la configuracion
         * de la pasarela de PayU Latam
         *
         * @access public
         * @return void
         */
        public function init_form_fields()
        {
            $this->form_fields = array(
                'enabled' => array(
                    'title' => __('Habilitar/Deshabilitar', 'wasistecredito'),
                    'type' => 'checkbox',
                    'label' => __('Habilita la pasarela de pago Sistecredito', 'wasistecredito'),
                    'default' => 'no'),
                'title' => array(
                    'title' => __('Título', 'wasistecredito'),
                    'type' => 'text',
                    'description' => __('Título que el usuario verá durante checkout.', 'wasistecredito'),
                    'default' => __('Sistecredito', 'wasistecredito')),
                'vendor_id' => array(
                    'title' => __('Vendor ID', 'wasistecredito'),
                    'type' => 'text',
                    'description' => __('ID único de usuario en Sistecredito.', 'wasistecredito')),
                'store_id' => array(
                    'title' => __('Store ID', 'wasistecredito'),
                    'type' => 'text',
                    'description' => __('ID único de tienda asignado por Sistecredito.', 'wasistecredito')),
                'subscription_key' => array(
                    'title' => __('Subscription Key', 'wasistecredito'),
                    'type' => 'text',
                    'description' => __('Token de consumo de REST API.', 'wasistecredito')),
                'gateway_url' => array(
                    'title' => __('Gateway URL', 'wasistecredito'),
                    'type' => 'text',
                    'description' => __('URL de la pasarela de pago Sistecredito.', 'wasistecredito')),
                'test' => array(
                    'title' => __('Transacciones en modo de prueba', 'wasistecredito'),
                    'type' => 'checkbox',
                    'label' => __('Habilita las transacciones en modo de prueba.', 'wasistecredito'),
                    'default' => 'no'),
            );
        }

        /**
         * Muestra el fomrulario en el admin con los campos de configuracion del gateway
         *
         * @access public
         * @return void
         */
        public function admin_options()
        {
            echo '<h3>' . __('Sistecredito Payment Gateway (by Waplicaciones)', 'wasistecredito') . '</h3>';
            echo '<table class="form-table">';
            $this->generate_settings_html();
            echo '</table>';
        }

        /**
         * Construye un arreglo con todos los parametros que seran enviados al gateway de Sistecredito
         *
         * @access public
         * @return void
         */
        public function get_params_post($order_id)
        {
            global $woocommerce;
            $order = new WC_Order($order_id);
            $documentType = isset($_POST["wasistecredito-document-type"]) ? $_POST["wasistecredito-document-type"] : "CC";
            $documentId = isset($_POST["wasistecredito-document-id"]) ? $_POST["wasistecredito-document-id"] : "";
            $curentDate = new DateTime();
            $transactionDate = $curentDate->format('Y-m-d\TH:i:sP');
            $amount = $order->get_total();

            $parameters_args = array(
                'typeDocument' => $documentType,
                'idDocument' => $documentId,
                'transactionDate' => $transactionDate,
                'valueToPaid' => $amount,
                'vendorId' => $this->vendor_id,
                'storeId' => $this->store_id,
                'orderId' => $order->id,
                'responseUrl' => home_url( '/' ) . 'wc-api/wasiscredito_confirmation',
            );

            return $parameters_args;
        }

        /**
         * Procesa el pago
         *
         * @access public
         * @return void
         */
        public function process_payment($order_id)
        {
            global $woocommerce;
            $order = wc_get_order($order_id);
            $sistecredito_api = new Sistecredito_Service_APIHandler($this->gateway_url);
            $rejectionUrl = add_query_arg(["order-failed" => $order_id, "key" => $order->order_key], get_permalink(wc_get_page_id("checkout")));

            if (version_compare(WOOCOMMERCE_VERSION, '2.0.19', '<=')) {
                wc_add_notice('error in woocomerce version', 'error');
                return array(
                    'result' => 'success',
                    'redirect' => $rejectionUrl,
                );
            } else {

                $parameters_args = $this->get_params_post($order_id);

                $response_url = $sistecredito_api->get_request_url($parameters_args, $this->settings);

                if (empty($response_url)) {
                    $response_url = $rejectionUrl;
                } else {
                    //DocumetadoAlejandro....
                    //$woocommerce->cart->empty_cart();
                }

                return array(
                    'result' => 'success',
                    'redirect' => $response_url,
                );
            }
        }

        /**
         * Retorna la configuracion del api key
         */
        function get_api_key()
        {
            return $this->settings['api_key'];
        }

        /**
         * Logging method.
         *
         * @param string $message Log message.
         * @param string $level Optional. Default 'info'. Possible values:
         *                      emergency|alert|critical|error|warning|notice|info|debug.
         */
        public static function log($message, $level = 'info')
        {
            if (self::$log_enabled) {
                if (empty(self::$log)) {
                    self::$log = wc_get_logger();
                }
                self::$log->log($level, $message, array('source' => 'wasistecredito'));
            }
        }

        public function confirm_payment()
        {
            $ecommerce_api = new Sistecredito_Adapter_WoocommercePlatformApi();
            $confirmation_controller = new Sistecredito_Controller_Confirmation($ecommerce_api);

            $confirmation_response = $confirmation_controller->process();
            //print_r($confirmation_response);exit;
            if ($confirmation_response->is_gateway_confirmation_request) {
                header("Content-Type: application/json");
                echo json_encode([
                    "status" => $confirmation_response->is_valid ? "VALID" : "ERROR",
                    "errors" => $confirmation_response->errors,
                ]);
                exit;
            }else{
                if(isset($confirmation_response->errors)){
                    $errores="";
                    foreach($confirmation_response->errors as $error){
                        if($errores!="")
                            $errores.="<br>";
                        $errores.=$error;
                    }
                    if($errores!=""){
                        wc_add_notice($errores, 'error');
                        wp_redirect(wc_get_checkout_url());
                        exit;
                    }
                }
            }
            
            $order = wc_get_order($confirmation_response->order->get_id());
            wp_redirect($this->get_return_url( $order ));
            exit;

        }
    }

    /**
     * Ambas funciones son utilizadas para notifcar a WC la existencia de Sistecredito
     */
    function add_wa_sistecredito($methods)
    {
        $methods[] = 'WC_Wapp_Sistecredito';
        return $methods;
    }
    add_filter('woocommerce_payment_gateways', 'add_wa_sistecredito');
}